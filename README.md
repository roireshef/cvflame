# CVFlame
Computer Vision Library for Scala (ready to use on top of Spark).
The Library currently supports face detection, 2D alignment (via eyes detection), and extraction.

## Prerequisites
This library depends on OpenCV (native c++, via JavaCPP) 
While the sbt-javacv plugin (in project/plugins.sbt) takes care off all JNI bindings, it is essential to insure the right version of OpenCV and its dependencies are are properly installed. If this library is used on top of Spark platform, OpenCV must be installed on each of the workers.

### Install OpenCV (on EMR)
On your EMR cluster, make sure all workers have opencv and gtk+ installed by including the bootstrap steps from
setup/opencv/install-gtk-devel.sh
setup/opencv/install-opencv-on-all-nodes.sh

### Copy CascadeClassifier XML files to all workers
Copy [haarcascade_frontalface_alt.xml](https://github.com/opencv/opencv/blob/master/data/haarcascades/haarcascade_frontalface_alt.xml) and [haarcascade_eye.xml](https://github.com/opencv/opencv/blob/master/data/haarcascades/haarcascade_eye.xml) to all workers. Put both files in an accessible (by OpenCV) folder

## Run face extraction (detection & alignment)
Import the implicits package: reshef.cv.utils.CVObjects.\_

Instantiate a new _reshef.cv.FaceDetector_ instance with the absolute local paths to _haarcascade_frontalface_alt.xml_ and _haarcascade_eye.xml_. Then use either:

1. reshef.cv.utils.Image.fromFile(filename: String)

2. reshef.cv.utils.Image.fromByteBuffer(filename: String)

to form a (opencv's) _Mat_ instance. Then you can use the _FaceDetector_ with its _extractFaces(imgMat: Mat)_ method to get a Seq of faces (opencv Mat instances).
