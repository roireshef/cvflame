name := "CVFlame"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "2.2.6" % "test"
)

javaCppPresetLibs ++= Seq("opencv" -> "3.1.0", "ffmpeg" -> "3.2.1")

javaCppPlatform := Seq("linux-x86_64")
javaCppVersion := "1.3.1"