package reshef.cv

import org.scalatest.FlatSpec
import reshef.cv.utils.ResourceManager
import utils.CVObjects._
import utils.Image
import org.bytedeco.javacpp.opencv_imgcodecs.imwrite
import java.nio.file.Files
import java.nio.ByteBuffer
/**
  * Created by roir on 12/12/2016.
  */
class TestFaceDetector extends FlatSpec {
  val outputDir = "C:\\temp"

  "FaceDetector" should "detect and align face from file" in{
    val imageName = "viber_user3.jpg"
    val imagePath = ResourceManager.getAbsolutePath(imageName,this.getClass)

    val imageMat = Image.fromFile(imagePath.toString)

    val faces = new FaceDetector().extractFaces(imageMat)
    faces.zipWithIndex.foreach{case (mat, i) =>
      val newFileName = imageName.split('.').mkString(s"_$i.")
      imwrite(Array(outputDir, newFileName).mkString("\\"), mat)
    }
  }

  "FaceDetector" should "detect and align face from bufferedimage" in{
    val imageName = "viber_user4.jpg"
    val imagePath = ResourceManager.getAbsolutePath(imageName,this.getClass)

    val byteArray = Files.readAllBytes(imagePath)
    val imageMat = Image.fromByteBuffer(ByteBuffer.wrap(byteArray))

    val faces = new FaceDetector().extractFaces(imageMat)
    faces.zipWithIndex.foreach{case (mat, i) =>
      val newFileName = imageName.split('.').mkString(s"_$i.")
      imwrite(Array(outputDir, newFileName).mkString("\\"), mat)
    }
  }
}
