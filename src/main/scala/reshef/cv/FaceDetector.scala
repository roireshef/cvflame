package reshef.cv

import java.nio.ByteBuffer

import org.bytedeco.javacpp.BytePointer
import org.bytedeco.javacpp.opencv_core.{Mat, _}
import org.bytedeco.javacpp.opencv_imgcodecs._
import org.bytedeco.javacpp.opencv_imgproc._
import org.bytedeco.javacpp.opencv_objdetect.CascadeClassifier
import reshef.cv.utils.CVObjects._
import reshef.cv.utils.ResourceManager

case class Eyes(left: Rect, right: Rect){
  @transient lazy val rotationAngle =
    math.atan2(right.center.y - left.center.y, right.center.x - left.center.x) * 180 / math.Pi

  def getRotationMat(numRows: Int, numCols: Int) = {
    val imgCenterPnt = new Point2f(numCols / 2f, numRows / 2f)
    imgCenterPnt.applyAndDeallocate(getRotationMatrix2D(_, rotationAngle, 1d))
  }
}


/**
  * A wrapper class for face detection and alignment in scala based on the opencv library
  * note that after compilation, the classifier xml files should be available to read by opencv
  * @param faceXMLPath a valid path to the classifier xml should be provided after compilation
  * @param eyesXMLPath a valid path to the classifier xml should be provided after compilation
  */
class FaceDetector(
                    faceXMLPath: String = ResourceManager.getAbsolutePath("haarcascade_frontalface_alt.xml").toString,
                    eyesXMLPath: String = ResourceManager.getAbsolutePath("haarcascade_eye.xml").toString){
  private val faceCascade = new CascadeClassifier(faceXMLPath)
  private val eyesCascade = new CascadeClassifier(eyesXMLPath)

  /*def extractFaces(filename: String): Seq[Mat] = {
    val imgMat = imread(filename)
    extractFaces(imgMat)
  }

  def extractFaces(buffer: ByteBuffer): Seq[Mat] = {
    val imgCvMat = cvDecodeImageM(cvMat(1, buffer.array().length, CV_8UC1, new BytePointer(buffer)));
    val imgMat = cvarrToMat(imgCvMat)

    //here, the original image is first resize to 320x320.
    // This should be changed according to the input data
    extractFaces(imgMat.applyAndDeallocate(_.resizeTo(320,320)))
  }*/

  def extractFaces(imgMat: Mat): Seq[Mat] = {
    val greyImgMat = imgMat.toGrayScale()
    equalizeHist(greyImgMat,greyImgMat)

    val faces = detectFaces(greyImgMat)

    val eyes = faces.
      map(_.withPadding(-0.1,imgMat.cols,imgMat.rows)).
      map(_.applyAndDeallocate(greyImgMat.apply)).
      map(detectEyes)

    //all elements of <faces> are deallocated internally
    val alignedFaces = (faces zip eyes).map{ case (faceRect, optEyes) =>
      val faceMat = faceRect.applyAndDeallocate(_.
        withPadding(0.15,imgMat.cols,imgMat.rows).
        applyAndDeallocate(greyImgMat.apply)
      )
      equalizeHist(faceMat,faceMat)

      //align (rotate) if eyes found
      optEyes.map(_.getRotationMat(faceMat.rows, faceMat.cols)).foreach(rMat =>
        rMat.applyAndDeallocate(warpAffine(faceMat, faceMat, _, new Size()))
      )

      val cropRect = new Rect(new Point(0,0), new Point(faceMat.cols, faceMat.rows)).
        applyAndDeallocate(_.withPadding(-0.1,imgMat.cols,imgMat.rows))

      cropRect.applyAndDeallocate(rect=>
        faceMat.applyAndDeallocate(_(rect))
      )
    }

    greyImgMat.deallocate()
    alignedFaces
  }

  protected def detectFaces(greyMat: Mat) = {
    val faceRects = new RectVector()
    faceCascade.detectMultiScale(greyMat, faceRects, 1.4, 6, 0, new Size(32,32), new Size(256,256))
    faceRects.applyAndDeallocate(_.toSeq)
  }

  protected def detectEyes(greyMat: Mat) = {
    val eyes = new RectVector()
    eyesCascade.detectMultiScale(greyMat, eyes, 1.1, 5, 0, new Size(), new Size())
    eyes.applyAndDeallocate(_.toSeq match{
      case seq if seq.length == 2 =>
        val srtdEyes = seq.sortBy(_.x())
        Some(Eyes(srtdEyes(0), srtdEyes(1)))
      case _ => None
    })
  }
}