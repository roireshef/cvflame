package reshef.cv.utils

import java.nio.ByteBuffer

import org.bytedeco.javacpp.opencv_core.{Mat, _}
import org.bytedeco.javacpp.{BytePointer, opencv_imgproc}
import org.bytedeco.javacpp.opencv_imgproc._
import org.bytedeco.javacv.CanvasFrame
import CVObjects._
import org.bytedeco.javacpp.opencv_imgcodecs._

/**
  * Created by roir on 12/12/2016.
  */
object Image {
  def show(imgMat: Mat, title: String): CanvasFrame = {
    val frame = new CanvasFrame(title)
    frame.showImage(imgMat.toFrame)

    val width = math.max(200,imgMat.rows)
    val scale = 1d*width/imgMat.rows
    val height = (scale * imgMat.cols).toInt
    frame.setCanvasSize(width, height)
    frame
  }

  def fromFile(filename: String) = imread(filename)

  def fromByteBuffer(buffer: ByteBuffer) = {
    val imgCvMat = cvDecodeImageM(cvMat(1, buffer.array().length, CV_8UC1, new BytePointer(buffer)))
    cvarrToMat(imgCvMat)
  }
}
