package reshef.cv.utils

import java.nio.file.Paths

/**
  * Created by roir on 12/12/2016.
  */
object ResourceManager {
  def getAbsolutePath(resource: String, parentClass: Class[_] = this.getClass) = {
    val winRegex = "(windows).*".r
    val path = parentClass.getClassLoader.getResource(resource).getPath
    (System.getProperty("os.name").toLowerCase match{
      case winRegex(str) => Paths.get(path.stripMargin('/'))
      case _ => Paths.get(path)
    }).toAbsolutePath
  }
}
