package reshef.cv.utils

import java.awt.image.BufferedImage

import org.bytedeco.javacpp.helper.opencv_core.AbstractCvScalar
import org.bytedeco.javacpp.opencv_core._
import org.bytedeco.javacpp.{Pointer, opencv_imgproc}
import org.bytedeco.javacpp.opencv_imgproc._
import org.bytedeco.javacv.OpenCVFrameConverter

/**
  * Created by roir on 12/12/2016.
  */
object CVObjects {
  trait AndDeallocate[+C <: Pointer] extends Pointer{
    protected def getObject(): C
    def applyAndDeallocate[T](f:(C)=>T) = {
      val result = f(getObject())
      this.deallocate()
      result
    }
  }

  implicit class ScalaRectSeq(rectVector: RectVector) extends AndDeallocate[RectVector]{
    override protected def getObject() = rectVector
    def toSeq: Seq[Rect] = for (i <- 0L until rectVector.size()) yield rectVector.get(i)
  }

  implicit class ScalaRect(rect: Rect) extends AndDeallocate[Rect]{
    override protected def getObject() = rect

    lazy val center = new Point(rect.x +  rect.width / 2, rect.y + rect.height / 2)
    def withPadding(percent: Double, xMax: Int, yMax: Int) = {
      val x = rect.x()
      val y = rect.y()
      val width = rect.width()
      val height = rect.height()

      new Rect(
        math.max(0,x - width * percent toInt),
        math.max(0,y - height * percent toInt),
        math.min(xMax,width * (1 + 2 * percent) toInt),
        math.min(yMax,height * (1 + 2 * percent) toInt)
        )
    }

    override def clone(): Rect = new Rect(rect)
  }

  implicit class ScalaImageMat(imgMat: Mat) extends AndDeallocate[Mat]{
    override protected def getObject() = imgMat

    def resizeTo(rows:Int, cols: Int) = {
      opencv_imgproc.resize(imgMat,imgMat, new Size(cols,rows))
      imgMat
    }

    def toFrame() = {
      val converter = new OpenCVFrameConverter.ToMat
      converter.convert(imgMat)
    }

    def toGrayScale(): Mat = {
      if (imgMat.channels() == 1) imgMat else {
        val greyMat = new Mat(imgMat.rows(), imgMat.cols(), CV_8U)
        opencv_imgproc.cvtColor(imgMat, greyMat, COLOR_BGR2GRAY, 1)
        greyMat
      }
    }

    def drawRectangle(r: Rect) = {
      rectangle(
        imgMat,
        new Point(r.x, r.y),
        new Point(r.x + r.width, r.y + r.height),
        new Scalar(AbstractCvScalar.RED),
        1,
        CV_AA,
        0
      )
    }

    def toBufferedImage(): BufferedImage = {
      val buffer = new BufferedImage(imgMat.cols(), imgMat.rows(),
        if (imgMat.channels() == 1) BufferedImage.TYPE_BYTE_GRAY else BufferedImage.TYPE_3BYTE_BGR)
      buffer.getRaster.setDataElements(0, 0, imgMat.cols(), imgMat.rows(), imgMat.toBytes())
      buffer
    }

    def toBytes(): Array[Byte] = {
      val ba = new Array[Byte](imgMat.total() * imgMat.channels() toInt)
      imgMat.data().get(ba)
      ba
    }

    def toGreyScaleMatrix(): Array[Array[Int]] = {
      val cvMat = new CvMat(imgMat)
      for (row <- 0 until cvMat.rows() toArray) yield
        for (col <- 0 until cvMat.cols() toArray) yield cvMat.get(row,col).toInt
    }

    def toRGBTensor(): Array[Array[Array[Int]]] = {
      val cvMat = new CvMat(imgMat)
      for (channel <- 0 until cvMat.channels() toArray) yield
        for (row <- 0 until cvMat.rows() toArray) yield
          for (col <- 0 until cvMat.cols() toArray) yield cvMat.get(row,col,channel).toInt
    }
  }

  implicit class ScalaPoint2f(point: Point2f) extends AndDeallocate[Point2f]{
    override protected def getObject(): Point2f = point
  }
}
